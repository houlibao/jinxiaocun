<?php
class _index
{
    public $companyConfigPath = '';
    public $goodsConfigPath = '';
    
    public function initc()
    {
        $this->companyConfigPath = CONFIGPATH.ENV.'/companyConfig.php';
        $this->goodsConfigPath = CONFIGPATH.ENV.'/goodsConfig.php';
    }
    
    public function getConfig($type = 1)
    {
        if ($type == 1) {
            include($this->companyConfigPath);
            return $companyConfig;
        } elseif ($type == 2) {
            include($this->goodsConfigPath);
            return $goodsConfig;
        }
        
    }
    
    public function index()
    {
        View::display('index');
    }

    public function companyList()
    {
        Response::success($this->getConfig(1));
    }
    
    public function goodsList()
    {
        $company = Request::Post('company');
        $goodsConfig = $this->getConfig(2);
        if (!empty($company) && !empty($goodsConfig[$company])) {
            
            if (empty($goodsConfig[$company])) {
                Response::error('公司不存在: '.$company);
            } else {
                Response::success(array($company => $goodsConfig[$company]));
            }
        }
        
        Response::success($goodsConfig);
    }
    
    public function addCompany()
    {
        $companyConfig = $this->getConfig(1);
        
        $company = Request::Post('company');
    
        if (in_array($company, $companyConfig)) {
            Response::error('公司已经存在: '.$company);
        }
        
        $companyConfig[] = $company;
        
        $str = '<?php'.PHP_EOL. '$companyConfig = ' . var_export($companyConfig, TRUE).';';
        file_put_contents($this->companyConfigPath, $str);
        
        FileLog::ini('', 'log')->prefix('添加公司')->caller(__METHOD__)->info($company);
        
        Response::success();
        
    }
    
    public function addGoods()
    {
        $companyConfig = $this->getConfig(1);
        $goodsConfig = $this->getConfig(2);
    
        $company = Request::Post('company');
        $goods = Request::Post('goods');
        $in = Request::Post('in', 0);
        $out = Request::Post('out', 0);
        
        if (!in_array($company, $companyConfig)) {
            Response::error('公司不存在: '.$company);
        }
        
        if(empty($goods)) {
            Response::error('产品不能为空');
        }
        
        if (!empty($goodsConfig[$company][$goods])) {
            Response::error('产品已经存在: '. $company .'-'. $goods);
        }
    
        $goodsConfig[$company][$goods] = array(
            'in' => $in,
            'out' => $out,
        );
    
        $str = '<?php'.PHP_EOL. '$goodsConfig = ' . var_export($goodsConfig, TRUE).';';
        file_put_contents($this->goodsConfigPath, $str);
    
        FileLog::ini('', 'log')->prefix('添加产品')->caller(__METHOD__)->info([$company, $goods]);
    
        Response::success();
    }
    
    public function addGoodsNum()
    {
        $goodsConfig = $this->getConfig(2);
        
        $company = Request::Post('company');
        $goods = Request::Post('goods');
        $in = Request::Post('in', 0);
    
        if (empty($goodsConfig[$company])) {
            Response::error('公司不存在: '.$company);
        }
    
        if(empty($goodsConfig[$company][$goods])) {
            Response::error('产品不存在: '.$goods);
        }
        
        $goodsConfig[$company][$goods]['in'] += $in ;
        
        $str = '<?php'.PHP_EOL. '$goodsConfig = ' . var_export($goodsConfig, TRUE).';';
        file_put_contents($this->goodsConfigPath, $str);
    
        FileLog::ini('', 'log')->prefix('添加进货数量')->caller(__METHOD__)->info([$company, $goods, $in]);
        
        Response::success($goodsConfig[$company][$goods]);
    }
    
    public function subGoodsNum()
    {
        $goodsConfig = $this->getConfig(2);
        
        $company = Request::Post('company');
        $goods = Request::Post('goods');
        $out = Request::Post('out', 0);
    
        if (empty($goodsConfig[$company])) {
            Response::error('公司不存在: '.$company);
        }
    
        if(empty($goodsConfig[$company][$goods])) {
            Response::error('产品不存在: '.$goods);
        }
    
        $goodsConfig[$company][$goods]['out'] += $out ;
        
        $str = '<?php'.PHP_EOL. '$goodsConfig = ' . var_export($goodsConfig, TRUE).';';
        file_put_contents($this->goodsConfigPath, $str);
    
        FileLog::ini('', 'log')->prefix('添加出货数量')->caller(__METHOD__)->info([$company, $goods, $out]);
        Response::success();
    }
    
    /**
     * 删除公司
     */
    public function delCompany()
    {
        $companyConfig = $this->getConfig(1);
        
        $company = Request::Post('company');
        $key = array_search($company, $companyConfig);
        if ($key) {
            unset($companyConfig[$key]);
            
            $str = '<?php'.PHP_EOL. '$companyConfig = ' . var_export($companyConfig, TRUE).';';
            file_put_contents($this->companyConfigPath, $str);
            FileLog::ini('', 'log')->prefix('删除公司')->caller(__METHOD__)->info($company);

            //删除公司下的产品
            $goodsConfig = $this->getConfig(2);
            unset($goodsConfig[$company]);
            $str = '<?php'.PHP_EOL. '$goodsConfig = ' . var_export($goodsConfig, TRUE).';';
            file_put_contents($this->goodsConfigPath, $str);
            FileLog::ini('', 'log')->prefix('删除公司所有产品')->caller(__METHOD__)->info([$company]);
    
            
            Response::success($companyConfig);
        } else {
            Response::error('未找到该公司');
        }
    }
    
    /**
     * 删除商品
     */
    public function delGoods()
    {
        $goodsConfig = $this->getConfig(2);
    
        $company = Request::Post('company');
        $goods = Request::Post('goods');
    
        if (empty($goodsConfig[$company])) {
            Response::error('公司不存在: '.$company);
        }
        
        if(empty($goodsConfig[$company][$goods])) {
            Response::error('产品不存在: '.$goods);
        }
        
        unset($goodsConfig[$company][$goods]);
    
        $str = '<?php'.PHP_EOL. '$goodsConfig = ' . var_export($goodsConfig, TRUE).';';
        file_put_contents($this->goodsConfigPath, $str);
    
        FileLog::ini('', 'log')->prefix('删除产品')->caller(__METHOD__)->info([$company, $goods]);
        
        Response::success();
    }
}