/**
 * Created by zhangzhibin on 2018/5/9.
 */

var url = 'http://' + window.location.host + '/';
/**
 * 添加公司
 */
function addCompany() {
    var company = prompt('输入供应商名字: ');
    company = company.replace(/\s/g, '');

    if (company.length > 1) {
        $.post(url+'addCompany', {company: company}, function (data) {
            //console.log(data);
            if (data.code === 1) {
                companyList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('公司或者产品名字为空');
    }

}

/**
 * 添加商品
 */
function addGoods() {
    var company = prompt('第1步, 输入公司名字: ');
    var goods = prompt('第2步, 输入产品名字: ');

    company = company.replace(/\s/g, '');
    goods = goods.replace(/\s/g, '');
    if (company.length > 1 || goods.length > 1) {
        $.post(url+'addGoods', {company: company, goods: goods}, function (data) {
            //console.log(data);
            if (data.code === 1) {
                goodsList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('供应商或者产品名字为空');
    }
}

/**
 * 添加商品数量
 */
function addGoodsNum(btn) {
    tr = btn.parentNode.parentNode;
    var company = tr.getAttribute('data-company-name');
    var goods = tr.getAttribute('data-goods-name');

    var num = prompt('输入商品数量:');
    num = num.replace(/\s/g, '');
    num = parseInt(num);

    if (num > 0) {
        $.post(url+'addGoodsNum', {company: company, goods: goods, in: num }, function (data) {
            //console.log(data);
            if (data.code === 1) {
                goodsList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('数量必须是大于零的整数');
    }
}

/**
 * 减少商品数量
 */
function subGoodsNum(btn) {
    tr = btn.parentNode.parentNode;
    var company = tr.getAttribute('data-company-name');
    var goods = tr.getAttribute('data-goods-name');

    var num = prompt('输入商品数量:');
    num = num.replace(/\s/g, '');
    num = parseInt(num);

    if (num > 0) {
        $.post(url+'subGoodsNum', {company: company, goods: goods, out: num }, function (data) {
            //console.log(data);
            if (data.code === 1) {
                goodsList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('数量必须是大于零的整数');
    }
}

/**
 * 删除供应商
 */
function delCompany() {
    var company = prompt('供应商名字: ');
    company = company.replace(/\s/g, '');
    if (company.length > 1) {
        $.post(url+'delCompany', {company: company}, function (data) {
            //console.log(data);
            if (data.code === 1) {
                companyList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('供应商名字为空');
    }
}

/**
 * 删除商品
 */
function delGoods() {
    var company = prompt('第1步, 输入供应商名字: ');
    var goods = prompt('第2步, 输入产品名字: ');

    company = company.replace(/\s/g, '');
    goods = goods.replace(/\s/g, '');

    if (company.length > 1 && goods.length > 1) {
        $.post(url+'delGoods', {company: company, goods: goods}, function (data) {
            //console.log(data);
            if (data.code === 1) {
                goodsList();
            } else {
                alert(data.msg);
            }
        });
    } else {
        alert('供应商或者产品名字为空');
    }
}

function goodsList(company_name) {

    var th = '<tr><th>产品</th><th>进货量</th><th>出货量</th><th>库存</th><th>操作</th></tr>';
    var tr = '<tr data-company-name="{companyname}" data-goods-name="{goodsname}">' +
        '<td>{goodsname}</td>' +
        '<td>{in}</td>' +
        '<td>{out}</td>' +
        '<td>{left}</td>' +
        '<td><button onclick="addGoodsNum(this)" >进货</button><button onclick="subGoodsNum(this)">出货</button></td></tr>';
    var caption = '<caption id="{cap_name}">{cap_name}</caption>';
    var table = '';
    $.post(url + 'goodsList', {company: company_name},function (msg) {
        //console.log(msg);
        for (company in msg.data) {
            var tpl_caption = caption.replace(/\{cap_name\}/g, company);
            //console.log(tpl_caption);

            var goodslist = msg.data[company];

            var tpl_trs = '';
            for (goods in goodslist) {
                var tmp_tr = tr;
                tmp_tr = tmp_tr.replace('{companyname}', company);
                tmp_tr = tmp_tr.replace(/\{goodsname\}/g, goods);
                tmp_tr = tmp_tr.replace('{in}', goodslist[goods].in);
                tmp_tr = tmp_tr.replace('{out}', goodslist[goods].out);
                tmp_tr = tmp_tr.replace('{left}', goodslist[goods].in - goodslist[goods].out);
                // console.log(tmp_tr);
                tpl_trs += tmp_tr;
            }

            //console.log(tpl_caption);

            table += tpl_caption + '<table>' + th + tpl_trs + '</table>';
        }

        document.getElementById('right_content').innerHTML = table;

    })
}

function companyList() {
    var tpl = '<tr><td>{name}</td></td></tr>';
    var tr = '';
    $.get(url + 'companyList', function (msg) {
        //console.log(msg);
        for (i in msg.data) {
            //console.log(msg.data[i]);
            var name = msg.data[i];
            var content = '<a onclick="goodsList(\''+name+'\')">' + name +'</a>';
            tr += tpl.replace('{name}', content);
        }

        var table = '<table>' + tr + '</table>';

        document.getElementById('left_sidebar').innerHTML = table;

    })
}
