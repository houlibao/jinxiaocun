<?php

class RouteConfig
{
    public static $Path = array(
        'companyList' => 'index/index/companyList',
        'goodsList' => 'index/index/goodsList',
        'addCompany' => 'index/index/addCompany',
        'addGoods' => 'index/index/addGoods',
        'delCompany' => 'index/index/delCompany',
        'delGoods' => 'index/index/delGoods',
        'addGoodsNum' => 'index/index/addGoodsNum',
        'subGoodsNum' => 'index/index/subGoodsNum',
    );
}