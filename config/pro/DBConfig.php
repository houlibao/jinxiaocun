<?php
class DBConfig
{
    //mysql link param
    public static $write = array(
        array(
            'host' => '127.0.0.1',
            'username' => '',
            'password' => '',
        )

    );

    public static $read = array(
        array(
            'host' => '127.0.0.1',
            'username' => '',
            'password' => '',
        )
    );
    
    public static $database = '';
    public static $table = '';

    //table info
    //虚拟表名 => 数据库名, 表名
    //最好将所有model在此备案, 方便管理
    public static $TableInfo = array(
        'company' => 'jcq, company',
        'goods' => 'jcq, goods',
    );

    public static function getDBInfo($modelName)
    {
        $strDT = '';
        if (array_key_exists($modelName, self::$TableInfo)) {
            $strDT = self::$TableInfo[$modelName];//获得database table 字符串
        } else {
            foreach (self::$TableInfo as $pattern => $dt) {
                if (strpos($pattern, '(') !== FALSE) {
                    preg_match('#'.$pattern.'#', $modelName, $matches);

                    if (!empty($matches)) {
                        $strDT = $dt;
                        foreach ($matches as $key => $value) {
                            $strDT = str_replace('$'.$key, $value, $strDT);
                        }
                        break;
                    }
                }
            }
        }

        if (!empty($strDT)) {
            $strDT = preg_replace('#\s+#', '', $strDT);//去掉空白
            $arr = explode(',', $strDT);
            self::$database = $arr[0];
            self::$table = $arr[1];
            return $arr;
        } else {
            self::error("未找到数据表 {$modelName} !");
        }
    }

    public static function error($msg)
    {
        exit($msg);
    }
}