<?php

class User extends Model
{
    public static $UserCookieName = 'SUMMERUSER';
    public static $UserLoginExpire = 1200;
    /**
     * 验证登录信息
     * @param string $mobile
     * @param string $password
     *
     * @return array
     */
    public static function verifyUserLogin($mobile, $password)
    {
        $user = self::link('user')
            ->fields('uid, username, mobile, password')
            ->where(['mobile' => $mobile])
            ->select()
            ->getOne();
        
//        FileLog::ini('login')->info([$mobile, $password, $user, Pet::$currentSql]);
        
        if (!empty($user)) {
            $bool = password_verify($password, $user['password']);
            if ($bool) {
                return $user;
            } else {
                return [];
            }
        } else {
            return [];
        }

    }

    /**
     * desc 根据手机号获取uid
     * @param $mobile
     * @param $field
     * @return string
     */
    public static function getUserInfoByMobile($mobile, $field='')
    {
        $rs = self::link('user')
            ->where(['mobile' => $mobile])
            ->select()
            ->getOne();

        return (!empty($field) && !empty($rs[$field])) ? $rs[$field] : $rs;
    }
    
    /**
     * desc 根据用户id获取用户信息
     * @param $uid
     * @param string $field
     * @return array|mixed
     */
    public static function getUserInfoById($uid, $field='')
    {
        $rs = self::link('user')
            ->fields('uid, username, addtime')
            ->where(['rowid' => $uid])
            ->select()
            ->getOne();
        
        return (!empty($field) && !empty($rs[$field])) ? $rs[$field] : $rs;
    }

    /**
     * desc 记录用户登录信息
     * @param $user
     * @param $expire
     */
    public static function setUserCookie($user, $expire=0)
    {
        $user['logintime'] = REQUEST_TIME;
        $json = json_encode($user);
        $secret = Safe::openssl($json);
        $arr = explode('.', $_SERVER['SERVER_NAME']);
        $domain = $arr[count($arr)-2].'.'.$arr[count($arr)-1]; //全域名有效
        
        if (empty($expire)) {
            setcookie(self::$UserCookieName, $secret, 0, '/', $domain, FALSE, TRUE); //关闭浏览器即失效
        } else {
            setcookie(self::$UserCookieName, $secret, time()+$expire, '/', $domain, FALSE, TRUE);
        }
        
    }

    /**
     * desc 获取用户信息
     * @param string $filed
     * @return string|array
     */
    public static function getUserCookie($filed='')
    {
        if (!empty(Request::Cookie(self::$UserCookieName))) {
            $secret = Request::Cookie(self::$UserCookieName);
            $json = Safe::openssl($secret, FALSE);
            $user = json_decode($json, TRUE);
            
            if (empty($user['logintime']) || (REQUEST_TIME - $user['logintime'] > self::$UserLoginExpire)) {
                return '';
            }
            
            if (!empty($filed) && !empty($user[$filed])) {
                return $user[$filed];
            } else {
                return $user;
            }
        } else {
            return '';
        }

    }

    /**
     * desc 清空用户登录信息
     */
    public static function clearUserCookie()
    {
        $arr = explode('.', $_SERVER['SERVER_NAME']);
        $domain = $arr[count($arr)-2].'.'.$arr[count($arr)-1]; //全域名有效
        setcookie(self::$UserCookieName, '-1', time()-3600, '/', $domain,FALSE, TRUE);
    }

}