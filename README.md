# 进销存

#### 项目介绍
基于浏览器的超简单的进销存本地运行软件

#### 软件架构
1. 精简版SummerPHP框架
2. 数据用文件存储未使用mysql等数据库, 存放在 jinxiaocun/config/companyConfig.php 和 jinxiaocun/config/goodsConfig.php
3. 对数据的增删改查日志记录在 jinxiaocun/log/log.log中

#### 安装教程

1. 下载官方PHP7.2源代码, 解压后文件夹命名为php72, 路径为D:/server/php72
2. 下载本代码, 解压后放在D:/server/code/jinxiaocun
3. 将本代码中的php72_9721start_inter.bat 复制到 D:/server/php72_9721start_inter.bat
4. 也可以放到E, F等盘

#### 使用说明

1. 双击 php72_9721start_inter.bat 启动PHP内置服务器
2. 在浏览器中输入 localhost:9721 即可


#### 文件结构
```
D:/server/
|---code/
|   |---jinxiaocun/
|       |---....
|       |---index.php       //nginx服务器入口文件
|       `---server.php      //PHP内置服务器入口文件
|---php72/              //PHP源码
`---php72_9721start_inter.bat   //启动PHP内置服务器脚本(windows)
```